<?php
	
	$IDATA['activity']=array(		
						
													'table'			=>'activity',
													'text'			=>'ACTIVITY',  
													'url'			=>'activity',
													'page_title'	=>'ADD NEW ACTIVITY',
													'page_content'	=>'activity',				
													'content'	=>array(
																			'task_id' 	=>array(
																							'type'=>'select',
																							'field'=>'task_id',
																							'label'	=>'TASK TITLE',
																							'value'=>'',
																							'mandatory'=>'1',
																							'max_length'=>'32',
																							'id'	=>'task_id',
																							'select_data'=>[
																								
																								'table_name'=>'task',
																								'id'	=>'task_id',
																								'pname'	=>'title',
																								'where'	=>'',
																								'order_by'=>''
																							],
																						),	

																			
																			'worked_hours'	=>array(
																						'field'=>'worked_hours',
																						'label'	=>'WORKED_HOUR',
																						'type'	=>'text',
																						'mandatory'=>'1',
																						'max_length'=>'16',
																						
																						'value'=>'',
																						'id'	=>'worked_hours'
																					),
																			'detail'=>array(
																						'field'=>'detail',
																						'label'	=>'DETAIL',
																						'type'	=>'textarea',
																						'id'	=>'detail',
																						'mandatory'=>'1',
																						'value'=>'',
																						'max_length'=>'256',
																						'rows'	=>"0",
																						'cols'	=>"20"
																						),
																			
																		
																			'submit'	=>array(
																						'label'	=>'submit',
																						'type'	=>'submit',
																						'id'	=>'submit'
																					),
																		
																		),							  
											'content_type'=>'form'
					
												
	);

?>
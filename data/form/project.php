<?php

	
		$IDATA['project']=array(							
			
													'table'			=>'project',
													'text'			=>'PROJECT',  
													'url'			=>'project',
													'page_title'	=>'ADD NEW PROJECT',
													'page_content'	=>'project',				
													'content'	=>array(
															
																			'title'	=>array(
																						'field'=>'title',
																						'label'	=>'TITLE',
																						'type'	=>'text',
																						'id'	=>'title',
																						'max_length'=>'32',
																						'mandatory'=>1,
																						 'value'=>''  
																					),
																			'start_date'=>array(
																						'field'=>'start_date',
																						'label'	=>'START_DATE',
																						'type'	=>'date',
																						'id'	=>'start_date',
																						'max_length'=>'64',
																						'mandatory'=>'1',
																						'value'=>''  
																					),
																			
																			'end_date'	=>array(
																						'field'=>'end_date',
																						'label'	=>'END_DATE',
																						'type'	=>'date',
																						'mandatory'=>'1',
																						'max_length'=>'16',
																						'id'	=>'end_date',
																						'value'=>''  
																					),
																			
																			'project_type' 	=>array(
																							'type'=>'select',
																							'field'=>'project_type',
																							'label'	=>'PROJECT TYPE',
																							'value'=>'',
																							'mandatory'=>'1',
																							'max_length'=>'32',
																							'id'	=>'project_type',
																							'select_data'=>[
																								
																								'table_name'=>'project_type',
																								'id'	=>'id',
																								'pname'	=>'name',
																								'where'	=>'',
																								'order_by'=>''
																							],
																						),
																		'client' 	=>array(
																							'type'=>'select',
																							'field'=>'client',
																							'label'	=>'CLIENT',
																							'value'=>'',
																							'mandatory'=>'1',
																							'max_length'=>'32',
																							'id'	=>'client',
																							'select_data'=>[
																								
																								'table_name'=>'client',
																								'id'	=>'id',
																								'pname'	=>'name',
																								'where'	=>'',
																								'order_by'=>''
																							],
																						),
																		'description'=>array(
																						'field'=>'description',
																						'label'	=>'DESCRIPTION',
																						'type'	=>'textarea',
																						'id'	=>'description',
																						'mandatory'=>'0',
																						'max_length'=>'128',
																						'rows'	=>"0",
																						'cols'	=>"20",
																						'value'=>''  
																						),
																		
																			'submit'	=>array(
																						'label'	=>'submit',
																						'type'	=>'submit',
																						'id'	=>'submit'
																					),
																		
																		),							  
											'content_type'=>'form'
				
								 
			
			);//for project
?>

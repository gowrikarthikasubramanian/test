<?php
	
	$IDATA['task']=array(		
													'table'			=>'task',
													'text'			=>'TASK',  
													'url'			=>'task',
													'page_title'	=>'ADD NEW TASK',
													'page_content'	=>'task',				
													'content'	=>array(
															
																		'id' 	=>array(
																							'type'=>'select',
																							'field'=>'id',
																							'label'	=>'PROJECT TITLE',
																							'value'=>'',
																							'mandatory'=>'1',
																							'max_length'=>'32',
																							'id'	=>'id',
																							'select_data'=>[
																								
																								'table_name'=>'project',
																								'id'	=>'id',
																								'pname'	=>'title',
																								'where'	=>'',
																								'order_by'=>''
																							],
																						),	
															
															
															
																			'title'	=>array(
																						'field'=>'title',
																						'label'	=>'TITLE',
																						'type'	=>'text',
																						'id'	=>'title',
																						'max_length'=>'32',
																						'value'=>'',
																						'mandatory'=>'1'
																					),
																			'assignees'	=>array(
																						'field'=>'assignees',
																						'label'	=>'ASSIGNEES',
																						'type'	=>'text',
																						'value'=>'',
																						'id'	=>'assignees',
																						'max_length'=>'32',
																						'mandatory'=>'1'
																					),
																			'status' 	=>array(
																							'type'=>'select',
																							'field'=>'status',
																							'label'	=>'STATUS',
																							'value'=>'',
																							'mandatory'=>'1',
																							'max_length'=>'32',
																							'id'	=>'status',
																							'select_data'=>[
																								
																								'table_name'=>'status',
																								'id'	=>'id',
																								'pname'	=>'name',
																								'where'	=>'',
																								'order_by'=>''
																							],
																						),	
																			'start_date'=>array(
																						'field'=>'start_date',
																						'label'	=>'START_DATE',
																						'type'	=>'date',
																						'value'=>'',
																						'id'	=>'start_date',
																						'max_length'=>'64',
																						'mandatory'=>'1'
																					),
																			
																			'end_date'	=>array(
																						'field'=>'end_date',
																						'label'	=>'END_DATE',
																						'type'	=>'date',
																						'value'=>'',
																						'mandatory'=>'1',
																						'max_length'=>'16',
																						'id'	=>'end_date'
																					),
																			
																			
																		'duration'	=>array(
																						'field'=>'duration',
																						'label'	=>'DURATION',
																						'type'	=>'text',
																						'id'	=>'duration',
																						'value'=>'',
																						'max_length'=>'32',
																						'mandatory'=>'1'
																					),
																			'submit'	=>array(
																						'label'	=>'submit',
																						'type'	=>'submit',
																						'id'	=>'submit'
																					),
																		
																		),							  
											'content_type'=>'form'
					

	);

?>
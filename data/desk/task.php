<?php

	
		$IDATA['task']=array(	

						'table'=>'task',
						
						'content'=>[
										'1'=> [
													'label'=> 'PROJECT TITLE',
													//'field'=>'id',
										     		'field'=>'(SELECT title FROM project WHERE id=task.id)',
													'sort'=>0
													
											],
										'2'=> [
													'label'=> 'TITLE',
										     		'field'=>'title',
													'sort'=>1
											],
										
										'3'=> [
													'label'=> 'ASSIGNEES',
										     		'field'=>'assignees',
													'sort'=>1
												],
										'4'=> [
													'label'=> 'STATUS',
													//'field'=>'status',
													'field'=>'(SELECT name FROM status WHERE id=task.status)',
													'sort'=>0
													
												],
												
										'5'=> [
													'label'=> 'START DATE',
										     		'field'=>'start_date',
													'sort'=>1
												],
										'6'=> [
													'label'=> 'END DATE',
										     		'field'=>'end_date',
													'sort'=>1
												],
										'7'=> [
													'label'=> 'DURATION',
										     		'field'=>'duration',
													'sort'=>1
												],
										'8'=> [
													'label'=> 'TIMESTAMP',
										     		'field'=>'timestamp_punch',
													'sort'=>1
												],
						
					
										],
							'delete'=>'delete',
										'update'=>'update',
										'primary_key'=>'id',
										'content_type'=>'display'
		
		
						);
?>
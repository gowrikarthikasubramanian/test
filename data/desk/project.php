<?php

	
		$IDATA['project']=array(	

						'table'=>'project',
						
						'content'=>[
						
										'1'=> [
													'label'=> 'TITLE',
										     		'field'=>'title',
													'sort'=>1	
													
											],
										'2'=> [
													'label'=> 'START DATE',
										     		'field'=>'start_date',
													'sort'=>1
												],
										'3'=> [
													'label'=> 'END DATE',
										     		'field'=>'end_date',
													'sort'=>1
												],
										'4'=> [
										
													'label'=> 'PROJECT TYPE',
										     		'field'=>'(SELECT name FROM project_type WHERE id=project.project_type)',
													'sort'=>0
												],
										'5'=> [
													'label'=> 'CLIENT',
										     		'field'=>'(SELECT name FROM client WHERE id=project.client)',
													'sort'=>0
												],
										'6'=> [
													'label'=> 'DESCRIPTION',
										     		'field'=>'description',
													'sort'=>1
												],
										'7'=> [
													'label'=> 'TIMESTAMP',
										     		'field'=>'timestamp_punch',
													'sort'=>1
												],
										
										
					
										],
						
										'delete'=>'delete',
										'update'=>'update',
										'primary_key'=>'id',
										'content_type'=>'display'
		           
		                       
		
						);
?>
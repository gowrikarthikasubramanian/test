<?php
        
                #include standard template file
		require("vendor/template.php");
		
		#include database config
		include 'db/config.php';
		
		#include parse
		include 'lib/parse.php';
		
		# include  form lib 
		include 'lib/form.php';
		
		#include  desk lib file
		include 'lib/desk.php';
				
		#include header 
		include 'data/header.php';
		
		#include footer
		include 'data/footer.php';
		
		#include menu 
		include 'data/menu.php';
                
?>
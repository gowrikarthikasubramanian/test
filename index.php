<?php
		#include all files
		include 'files.php';
		
		
		#temp variable 
		$page_con	='';      //for display page content 
		$form_key='';    
		$desk_key='';
		#temp variable as array
		$display_table		=array();
		$form_build 		=array();
		$sort_data			=array();
		$IDATA = [];
			
				
				
					
				$form_key   =  (@$_GET['form'])?$_GET['form']:'';

				$desk_key   =  (@$_GET['desk'])?$_GET['desk']:'';
				
				
				
				
				
				if($form_key)
				{
						include("data/form/$form_key.php");
						
						$content = $IDATA[$form_key];
						
						if(@$_GET['update'])
						{
						   	
							$content=show_data($content);
							
							if(isset($_POST['submit'])){
								
								update_data($content);
							}
							
						}
						
						else if($content['content_type']=="form")
						{
			
								if(isset($_POST['submit'])){
			
									$form_build =form_validate($content);
								}
						
						
						}
						
						$page_con=$parse[$content['content_type']]($content['content']);								
						
				}
				
				if($desk_key)
				{
					
						include("data/desk/$desk_key.php");

						$content = $IDATA[$desk_key];
						
						$sort_data=sort_field($content);
						
						
						if(@$_GET['delete'])
						{
						   	
							delete(['table'=>$content['table'],
							       'primary_key'=>$content['primary_key'],
							       'value'=>$_GET['delete']
							]);
							
						}
					
						
						
								$display_table	=retrive($desk_key,$content);	
						
						
						
				}
				
		
			include 'db/close.php';
		
			#templating
			$T =& new Template("template/index.html");
	
			$T->AddParam('TITLE','PROJECT MANAGEMENT SYSTEM');		
			$T->AddParam($HEADER);
			$T->AddParam('MENU_INFO',$MENU);
			
			
			$T->AddParam('CONTENT',$page_con);
			
			
			$T->AddParam('FOOTERTITLE','Address');
			$T->AddParam($FOOTER);
			
			if($form_build){
				$T->AddParam($form_build);
			}
		
			if($display_table){
				$T->AddParam('DESK_KEY',$desk_key);
				$T->AddParam('RESULT',$display_table);
				
			}
			
			if($sort_data){
				$T->AddParam('SORT_DATA',$sort_data);
			}
			
				
			$T->EchoOutput();
			
			
?>				

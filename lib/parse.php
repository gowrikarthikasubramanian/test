<?php		
//start of form function
	
		$parse['form']=function($param){
				
				global $parse;
					
				$lv		=array();
							
				$lv['content'] = '';
					
					foreach($param as $form_key=>$form_data)
					{
						if($form_data['label']!="submit")
						{
								$lv['content'].="<tr><td><b>$form_data[label]</b></td></tr>";
						}		
						$lv['content'].=$parse[$form_data['type']]($form_data);
						
					}
																																												
				return  '<form method="POST"><table border="1">'.$lv['content'].'</table></form>';
										
		}; //end of form  

//start of select function		
		$parse['select']=function($param){
			
			global $CON;
			
			$lv		=array();
			$type      = array();
			
			$lv['content']	='';
			$temp_id   	= "";
			$temp_name 	= "";
			$lv['val_text'] = '';
			
			$lv['content'].= "<tr><td>".
			                 "<select id='$param[id]' name='$param[id]' maxlength='$param[max_length]'>";
			
			$data= $param['select_data'];
			
			$opt_query="SELECT
													$data[id],$data[pname]
								          FROM
													$data[table_name]";
												
			$opt_res = mysqli_query($CON,$opt_query);
			
			$temp_id=   $data['id'];
			$temp_name= $data['pname'];
			
			$lv['content'].="<option>--Select--</option>";
			
			while($ptype=mysqli_fetch_array($opt_res)){
				
				
				$sel= ($ptype[$temp_id]==$param['value'])?'selected':'';
				
				$lv['content'].="<option value='$ptype[$temp_id]' $sel>$ptype[$temp_name]</option>";
			}
			
			
			$lv['content'].= "</select>".
			                 "</td></tr>";
			
			return $lv['content'];
		
		}; 

//start of text function		
		$parse['text']=function($param){
			
			$lv=array();
			$lv['val_text']	='';
			$lv['content'] 	= '';
			
			foreach($param as $form_key=>$form_data){
						
				if($form_data=="text")
				{
					$lv['val_text']=($param['value'])?" value='$param[value]'":'';	
						
					$lv['content'].= "<tr><td><input id='$param[id]' name='$param[id]'  type='$form_key' ".
					                 " $lv[val_text] maxlength='$param[max_length]'></td></tr>";								
																								
				}
			}
			
			return  $lv['content'];
		}; //end of text function

//start of textarea function
		$parse['textarea']=function($param){
				
				global $CON;
				
				$lv['val_text']	='';
				$lv		=array();
				$lv['content'] 	= '';
				
				foreach($param as $form_key=>$form_data){
					
					if($form_data=="textarea")
					{
						
						$v = mysqli_escape_string($CON,$param['value']);
						$lv['val_text']=($param['value'])?"$v":'';
						$val="textarea";
						
						$lv['content'].= "<tr><td><$val rows='$form_key' cols='$form_key' id='$param[id]' name='$param[id]' 
				 maxlength='$param[max_length]'>$lv[val_text]</$val></td></tr>";								
																														
					}
				}
				
				return  $lv['content'];
		}; //end of textarea function
		
//start of button function
		$parse['button']=function($param){
			
			$lv		=array();		
			$lv['content'] 	= '';
			
		        $value		=$param['type'];
		        $lv['content'].= "<tr><td><input type='$value' id='$param[id]' name='$param[id]' value='$param[label]'></td></tr>";						
					
			
			return  $lv['content'];
		
		}; //end of button function
		$parse['submit']=$parse['button'];
		
//start of date function
		$parse['date']=function($param){
			
			$lv		=array();
			$lv['val_text']	='';
			$lv['content'] 	= '';
			
			foreach($param as $form_key=>$form_data){
				
				if($form_data=="date")
				{
					$lv['val_text']=($param['value'])?" value='$param[value]'":'';	
					$lv['content'].= "<tr><td><input id='$param[id]' name='$param[id]'".
					                 " $lv[val_text] type='$form_data'>
					
					</td></tr>";								
																								
				}
			}
			return  $lv['content'];
		}; //end of text function


?>



<?php
//start of desk table
//i/o header,data
	function retrive($desk,$param)
	{
			global $CON;
			
			
			$info 			= array();
			$lv 			= array();
			$lv['field']		=[];
			$lv['header']		=[];
			$lv['primary']		=[];
			$lv['search_query']	="";
			$lv['sort_query']	="";
			$temp			=[];
			$show			= [];
			$temp_header		=0;
			
			if(!isset($_GET['submit']))
			{
				foreach($param['content'] as $key=>$value)
				{	
					$temp_header++;
					$temp['col_'.$temp_header]=$value['label'];
					
					array_push($lv['field'],$value['field']);
						
				}
				
				array_push($lv['field'],$param['primary_key']);
				
				// header
				array_push($temp,'ACTION');
				array_push($lv['header'],$temp);
	   
			}
			//search
			if(@$_GET['sort'])
			{
				$lv['sort_query']="ORDER BY $_GET[sel_sort] desc" ;
			}
			if(@$_GET['search'])
			{
				$lv['sort_query']="WHERE $_GET[search_sort]='$_GET[search_text]'" ;
			}
			
			$sql = "select 
			
								".join(",",$lv['field'])."
					 from 
					 			$param[table]
				
								$lv[sort_query]";
				
					
			$result=mysqli_query($CON,$sql);
			
	
			while ($row_info = mysqli_fetch_assoc($result)){
			
			
				$row_info['id']=action_builder($desk,$row_info,$param); 	
				array_push($info,$row_info);
						
			}
			
			$show['h']=show($lv['header']);
			$show['i']=show($info);
			$show=array_merge($show['h'],$show['i']);
			
			return  $show;
			
		
}  //end of desk table

//start of action builder

	function action_builder($desk,$row_info,$param)
	{
		
		$result="<a href='?desk=$desk&delete=$row_info[id]'>"."Delete"."</a>".
		 "&nbsp;|&nbsp;".
		 "<a href='?form=$param[table]&update=$row_info[id]'>"."Update"."</a>";		  																		
		return $result;								
	
	} // end of action builder
	
		
	
//start of show data as table view
//i/o:data as array of array

	function show($data){
									
						
						$row_info   = array();
						
						
						foreach($data as $row_key=>$row_value){
			
					
						$col_info  = array();
							
							foreach($row_value as $col_key=>$col_value){
					
								
								array_push($col_info,['col'=>$col_value]);
								
							}
							
							array_push($row_info,['row_info'=>$col_info]);
						}
						
						return $row_info;
						
						
				 //end of for each 
												
		} // end of show data as table view
		
//start of delete function
	function delete($param)
	{
			
			global $CON;
			
			$sql = "delete 
								
					 from 
					 		$param[table]	
								
					 where
							$param[primary_key]=$param[value]	";
						
											
						$result=mysqli_query($CON,$sql);
						
						if(! $result ) {
							 die('Could not delete data: ' . mysql_error());
						}else{
            
							echo "Deleted data successfully\n";
						}
	} //end of delete function
	
//start of update function

function update_data($param)
{
		     
		       global $CON;
			
			$lv 			= array();
			$lv['form_data']	=[];
			$lv['value']		='';
			$lv['field']		='';
			$lv['res']		=array();
		       
			$table_name		=$param['table'];
				
			foreach($param['content'] as $idx=>$value)
			{
				
				if($idx!="submit"){
						
					$lv['field']=$idx;
					
					$lv['value']=$_POST[$idx];
					
					array_push($lv['res'],"$idx='".$_POST[$idx]."'");
				
				}
		
			}	
		
			$sql = "UPDATE
								$table_name
								
						SET
				
								".implode(",",$lv['res'])."	
								
						
						WHERE
								id=$_GET[update]";
								
			$result=mysqli_query($CON,$sql);
		
			if (!$result) {
					echo "Error: " . $sql . "<br>" . mysqli_error($CON);
					    
							
					} else {
			
						
					echo "Updated successfully";
			}
				
		
		
} //end of update function

//start of show_data function

		function show_data($param)
		{
		
		       global $CON;
			
			$lv 			= array();
			
			$lv['form_data']	=[];
			
			$lv['field']		=[];
		        $result_array 		= array();
			$table_name		=$param['table'];
				
			foreach($param['content'] as $idx=>$value)
			{
				if($idx!="submit"){
				
					array_push($lv['field'],$value['field']);
				}
				
				
			}
			$sql = "select 
			
								".join(",",$lv['field'])."
					 from 
					 			$param[table]
					
			where
						id=$_GET[update]";
			
			
				
		
				$result=mysqli_query($CON,$sql);
		
				if (!$result) {
					echo 'Could not run query: ' . mysql_error();
			 
				}
			
				
				while($row = mysqli_fetch_assoc($result))
				{
				
					foreach($param['content'] as $idx=>$value)
					{
				
						if($idx!="submit"){
					
							$param['content'][$idx]['value']=$row[$idx];
							
					}
				
				
				
					}
				}
				
		return $param;
		
		
	} //end of show_data

	
//start of sort_field

	function sort_field($param)
	{
		
					$lv		=array();
							
					$lv['label'] 	=[];
					$lv['field'] 	=[];
					
					foreach($param['content'] as $form_key=>$form_data)
					{
					
						if($form_data['sort']==1)
						{
					
							if($form_key!="submit"){
							array_push($lv['label'] ,['label'=>$form_data['label'],
													'field'=>$form_data['field']]);
							
							}
						
					
						}
				
					}
																																											
				return $lv['label'] ;
										
		} //end of sort_field function
		
//start of sort_data

		
?>